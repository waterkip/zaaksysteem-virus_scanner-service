use Test::More;
use Test::Compile;

Test::Compile->new->all_files_ok;

done_testing;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

Zaaksysteem::Virus_scanner uses the EUPL license, for more information please have a look at the C<LICENSE> file.

