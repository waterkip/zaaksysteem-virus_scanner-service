FROM registry.gitlab.com/zaaksysteem/zaaksysteem-service-http-library:51123e8b

ENV SERVICE_HOME /opt/zaaksysteem-virus_scanner-service

WORKDIR $SERVICE_HOME

ENV ZSVC_LOG4PERL_CONFIG=/etc/zaaksysteem-virus_scanner-service/log4perl.conf \
    ZSVC_SERVICE_CONFIG=/etc/zaaksysteem-virus_scanner-service/zaaksysteem-virus_scanner-service.conf

COPY Makefile.PL ./
COPY bin ./bin
COPY etc /etc/zaaksysteem-virus_scanner-service
COPY lib ./lib
COPY t ./t
COPY xt ./xt

RUN cpanm --installdeps . && rm -rf ~/.cpanm

CMD starman $SERVICE_HOME/bin/zaaksysteem-virus_scanner-service.psgi

