package Zaaksysteem::Virus_scanner::Service;

use Moose;

use version; our $VERSION = version->declare('v0.0.1');

extends 'Zaaksysteem::Service::HTTP';

=head1 NAME

Zaaksysteem::Virus_scanner::Service - One-line description here

=head1 DESCRIPTION

Description here

=head1 SYNOPSIS

    use Zaaksysteem::Virus_scanner::Service;

    # Example usage here

=cut

use BTTW::Tools;

# Code here

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

Zaaksysteem::Virus_scanner uses the EUPL license, for more information please have a look at the C<LICENSE> file.

