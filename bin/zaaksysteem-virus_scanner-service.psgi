#!/usr/bin/perl -w

use strict;

use FindBin;
use lib "$FindBin::Bin/../lib";

use Zaaksysteem::Virus_scanner::Service;

my %args;

if (exists $ENV{ ZSVC_LOG4PERL_CONFIG }) {
    $args{ log4perl_config_file } = $ENV{ ZSVC_LOG4PERL_CONFIG };
}

if (exists $ENV{ ZSVC_SERVICE_CONFIG }) {
    $args{ service_config_file } = $ENV{ ZSVC_SERVICE_CONFIG };
}

Zaaksysteem::Virus_scanner::Service->new(%args)->to_app;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

Zaaksysteem::Virus_scanner uses the EUPL license, for more information please have a look at the C<LICENSE> file.

